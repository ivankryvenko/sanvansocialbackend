const User = require('../services/user.service');

exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: `Request body shouldn't be empty`
        });
    }

    const newUser = {
        first_name: req.body.firstName,
        last_name: req.body.lastName,
        email: req.body.email,
        password: req.body.password
    };

    User.createUser(newUser, (err, data) => {
        if (err) {
            res.status(err.status).send({
                message: err.message,
            });
        } else {
            res.status(data.status).send({
                message: "Registration success!",
                data: JSON.stringify(data.newUser, null, 2),
            });
        }
    })
};

exports.login = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: `Request body shouldn't be empty`
        });
    }

    const user = {
        email: req.body.email,
        password: req.body.password,
    }

    User.logIn(user, (err, data) => {
        if (err) {
            res.status(err.status).send({
                message: err.message,
            });
        } else {
            res.status(data.status).send({
                message: "Successfully logged in",
                body: JSON.stringify(data.existedUser)
            });
        }
    });
}