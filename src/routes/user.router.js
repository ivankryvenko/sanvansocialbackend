module.exports = app => {
    const user = require('../controllers/user.controller');

    app.post('/register', user.create);
    
    app.post('/login', user.login);

    app.get('/user', (req, res) => {
        res.send('Hi');
    });
};