const mongoose = require('mongoose');

const { MONGO_URI } = process.env;

exports.connect = () => {
    mongoose
        .connect(MONGO_URI, {
            useNewUrlParser: true,
        })
        .then(() => {
            console.log('Successfully connected to database');
        })
        .catch((err) => {
            console.log('DB connection failed');
            console.error(err);
            process.exit(1);
        });
}