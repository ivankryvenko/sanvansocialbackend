const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

User.createUser = async (newUserData, result) => {
    try {
        const { first_name, last_name, email, password } = newUserData;

        if (!(first_name && last_name && email && password)) {
            console.log('not all fields were entered');
            result({
                status: 400, 
                message: "Not all fields were entered"
            }, null);
            return;
        }

        //checking if we already have a user with that email
        const oldUser = await User.findOne({ email });

        if (oldUser) {
            console.log('User already exists');
            result({
                status: 400, 
                message: "User already exists"
            }, null);
            return;
        }

        const salt = await bcrypt.genSalt(10);  
        const encryptedPassword = await bcrypt.hash(password, salt);

        const newUser = await User.create({
            first_name,
            last_name,
            email: email.toLowerCase(),
            password: encryptedPassword,
        });

        const token = jwt.sign(
            {
                user_id: newUser._id,
                firstName: first_name,
                lastName: last_name, 
                email
            },
            process.env.TOKEN_KEY,
            {
                expiresIn: "5h",
            }
        );

        newUser.token = token;

        result(null, {status: 201, newUser});
    } catch(err) {
        console.log(err);
    }
};

User.logIn = async (user, result) => {
    try {
        const { email, password } = user;

        if (!(email && password)) {
            console.log('Email and password are required!');
            result({
                status: 400, 
                message: 'Email and password are required'
            }, null);
            return;
        }

        const existedUser = await User.findOne({ email });

        if (existedUser && (await bcrypt.compare(password, existedUser.password))) {
            const token = jwt.sign(
                { 
                    user_id: user._id, 
                    email 
                },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "5h",
                }
            );

            existedUser.token = token;

            result(null, {
                status: 200,
                existedUser
            });
            return;
        }

        result({
            status: 404, 
            message: "User not found"
        });
    } catch (err) {
        console.log(err);
    }
};

module.exports = User;