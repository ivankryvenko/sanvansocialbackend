const express = require('express');
require('dotenv').config();

require("./src/config/db").connect();

const app = express();

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.get('/', (req, res) => {
    res.send('Hello World!');
});

require('./src/routes/user.router')(app);

module.exports = app;